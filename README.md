# SDL_Library

Compile using: `g++ Calculator.cpp -o Calculator -lSDL3`.

In case of something going wrong: `pkg-config sdl3 --cflags --libs`.

Run using: `./Calculator`
