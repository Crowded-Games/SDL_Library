#include <SDL3/SDL.h>
#include <vector>
#include <cstdio>

class Button
{
public:
    SDL_FRect button;

	struct
    {
        Uint8 r = 255;
        Uint8 g = 255;
        Uint8 b = 255;
        Uint8 a = 255;
    } standard;
    struct
    {
        Uint8 r = 127;
        Uint8 g = 127;
        Uint8 b = 127;
        Uint8 a = 255;
    } hover;

    Button(int XPosition, int YPosition, int Width, int Height)
    {
		button.h = Height;
		button.w = Width;
		button.x = XPosition;
		button.y = YPosition;
	}

    void PressButton(SDL_Event event) {
		// If it's within the bounds
		if (event.button.x >= button.x && event.button.x <= (button.x + button.w) && event.button.y >= button.y && event.button.y <= (button.y + button.h))
			{
				printf("Button has been pressed \n");
			}
	}
	
    void ProcessButton(SDL_Renderer * renderer, SDL_Event event) {
		// Get the mouse position.
		float x, y;
		SDL_GetMouseState(&x, &y);
		// If it's within the bounds
		if  (x >= button.x && x <= (button.x + button.w) && y >= button.y && y <= (button.y + button.h))
			{
				SDL_SetRenderDrawColor(renderer, hover.r, hover.g, hover.b, hover.a);
			}
		else
			{
				SDL_SetRenderDrawColor(renderer, standard.r, standard.g, standard.b, standard.a);
			}
		SDL_RenderFillRect(renderer, &button);
	}
};

class Application
{
public:
	SDL_Window *Window;
    SDL_Event WindowEvent;
    SDL_Renderer *WindowRender;
    
    std::vector<Button> buttons;
    
    Application()
    {
		SDL_CreateWindowAndRenderer("Window", 680, 480, 0, &Window, &WindowRender);
	}

    void Update() {
		bool KeepWindowOpen = true;
		while(KeepWindowOpen)
		{
			while(SDL_PollEvent(&WindowEvent))
			{
				switch(WindowEvent.type)
				{
					case SDL_EVENT_QUIT:
						KeepWindowOpen = false;
						break;
					case SDL_EVENT_MOUSE_BUTTON_DOWN:
						for (int i = 0; i < buttons.size(); i++)
						{
							buttons[i].PressButton(WindowEvent);
						}
						break;
				}
			}
		SDL_SetRenderDrawColor(WindowRender, 0, 0, 0, 255);
        SDL_RenderClear(WindowRender);
        for (long unsigned int i = 0; i < buttons.size(); i++)
		{
			buttons[i].ProcessButton(WindowRender, WindowEvent);
		}
		SDL_RenderPresent(WindowRender);
		}
	}
	
    void AddButton(Button butt)
    {
		buttons.push_back(butt);
	}
};
